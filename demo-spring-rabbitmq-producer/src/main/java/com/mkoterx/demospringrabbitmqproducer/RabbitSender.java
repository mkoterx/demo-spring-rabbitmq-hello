package com.mkoterx.demospringrabbitmqproducer;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.core.DirectExchange;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.UUID;

@Component
public class RabbitSender {

    private Logger logger = LoggerFactory.getLogger(RabbitSender.class);

    private final RabbitTemplate rabbitTemplate;
    private final DirectExchange directExchange;
    private final Queue workQueue;
    private final Queue resultQueue;

    public RabbitSender(RabbitTemplate rabbitTemplate,
                        @Qualifier("workQueue") Queue workQueue,
                        Queue resultQueue,
                        DirectExchange directExchange) {
        this.rabbitTemplate = rabbitTemplate;
        this.workQueue = workQueue;
        this.resultQueue = resultQueue;
        this.directExchange = directExchange;
    }

    @Scheduled(fixedDelay = 5000L)
    public void send() {
        String message = UUID.randomUUID().toString().substring(0, 5);
        logger.info("Sending job [{}] to work queue ...", message);
        rabbitTemplate.convertAndSend(directExchange.getName(), workQueue.getName(), message, m -> {
            m.getMessageProperties().setHeader("result-routing-key", resultQueue.getName());
            return m;
        });
    }

    @RabbitListener(queues = "#{resultQueue.getName()}") // TODO harcoded!!!
    public void receiveJobResult(String result) {
        logger.info("Job successfully completed: [{}]", result);
    }
}
