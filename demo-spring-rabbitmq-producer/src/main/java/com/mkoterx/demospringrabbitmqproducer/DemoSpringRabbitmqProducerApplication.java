package com.mkoterx.demospringrabbitmqproducer;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableScheduling
public class DemoSpringRabbitmqProducerApplication {

	public static void main(String[] args) {
		SpringApplication.run(DemoSpringRabbitmqProducerApplication.class, args);
	}
}
