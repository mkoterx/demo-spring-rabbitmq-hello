package com.mkoterx.demospringrabbitmqproducer;

import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.DirectExchange;
import org.springframework.amqp.core.Queue;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class RabbitConfig {

    @Value("${spring.application.name}")
    private String producerId;

    @Bean
    public DirectExchange directExchange() {
        return new DirectExchange("mkoterx.exchange.direct");
    }

    @Bean("workQueue")
    public Queue workQueue() {
        return new Queue("mkoterx.queue.work");
    }

    @Bean
    public Binding bindingWorkQueueDirectExchange(DirectExchange directExchange,
                                                  Queue workQueue) {
        return BindingBuilder.bind(workQueue).to(directExchange).withQueueName();
    }

    @Bean("resultQueue")
    public Queue resultQueue() {
        return new Queue("mkoterx.queue.result." + producerId);
    }

    @Bean
    public Binding bindingResultQueueDirectExchange(DirectExchange directExchange,
                                                    Queue resultQueue) {
        return BindingBuilder.bind(resultQueue).to(directExchange).withQueueName();
    }


}
