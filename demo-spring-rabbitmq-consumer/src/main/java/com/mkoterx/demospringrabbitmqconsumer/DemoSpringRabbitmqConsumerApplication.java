package com.mkoterx.demospringrabbitmqconsumer;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DemoSpringRabbitmqConsumerApplication {

	public static void main(String[] args) {
		SpringApplication.run(DemoSpringRabbitmqConsumerApplication.class, args);
	}
}
