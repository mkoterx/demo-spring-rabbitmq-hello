package com.mkoterx.demospringrabbitmqconsumer;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.messaging.handler.annotation.Header;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Component;

@Component
@RabbitListener(queues = "mkoterx.queue.work")
public class RabbitConsumer {

    private Logger LOGGER = LoggerFactory.getLogger(RabbitConsumer.class);

    @Value("${spring.application.name}")
    private String appName;

    private final RabbitTemplate rabbitTemplate;

    public RabbitConsumer(RabbitTemplate rabbitTemplate) {
        this.rabbitTemplate = rabbitTemplate;
    }

    @RabbitHandler
    public void process(@Payload String payload, @Header(name = "result-routing-key") String resultRoutingKey) {
        LOGGER.info("Message received by [{}], message: [{}]", appName, payload);
        LOGGER.info("Sending result to [{}]...", resultRoutingKey.substring(resultRoutingKey.lastIndexOf(".") + 1, resultRoutingKey.length()));
        rabbitTemplate.convertAndSend("mkoterx.exchange.direct", resultRoutingKey, payload + ":result");
    }
}
